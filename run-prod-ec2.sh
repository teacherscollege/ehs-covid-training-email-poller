#!/usr/bin/env bash
export EHS_EMPL_SHEET=EHS-tracker-employee-PROD
export EHS_STUD_SHEET=EHS-tracker-student-PROD
export SERVICE_ACCOUNT_CREDS=$SERVICE_ACCOUNT_CREDS
export DELEGATED_CREDS=$DELEGATED_CREDS
export LOGFILE=/home/ec2-user/ehs-covid-training-email-poller/email-poller.log
/home/ec2-user/ehs-covid-training-email-poller/venv/bin/python /home/ec2-user/ehs-covid-training-email-poller/app.py
