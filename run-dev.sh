#!/usr/bin/env bash
export EHS_EMPL_SHEET=EHS-tracker-employee-DEV
export EHS_STUD_SHEET=EHS-tracker-student-DEV
export SERVICE_ACCOUNT_CREDS=$(cat ea-dev-proxy-api-service-account-creds.json)
export DELEGATED_CREDS=$(cat ea-dev-proxy-delegated-creds.json)
python app.py
