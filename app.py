from datetime import datetime
import base64
import os
import json
import logging
from logging.handlers import RotatingFileHandler

import gspread
from google.oauth2 import service_account
from google.oauth2.credentials import Credentials
from googleapiclient.discovery import build
from googleapiclient.discovery_cache.base import Cache

# imports for the google code sample
import pickle
import os.path
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request

# other imports
import pandas as pd
import toolz
from dateutil import parser
from dateutil import tz

# Configuration values
# load the service account credentials. these are the google credentials used to
# gain access to the spreadsheets that this script is responsible for updating.
_CREDS = os.environ["SERVICE_ACCOUNT_CREDS"]
CREDS = json.loads(_CREDS)
# if you need to prepare the delegated email addresses oauth credentials, set
# the PREPARE_OAUTH_CREDS env var to 1. this will require a web browser in order
# to accept the OAuth. you must also have access to the delegated email addresses
# google accounts for you to be able to do this.
# other wise we will be loaded the delegated credentials directly, and assume the
# OAuth process has already take place.
PREPARE_OAUTH_CREDS = int(os.getenv("PREPARE_OAUTH_CREDS", 0))
if PREPARE_OAUTH_CREDS == 1:
    JSON_CREDS_CACHE = os.environ["JSON_CREDS_CACHE"]
    if not os.path.exists(JSON_CREDS_CACHE):
        # These are only needed when generating the credentials for the
        # the first time using the OAuth process.
        _OAUTH_CREDS = os.environ["OAUTH_CREDS"]
        OAUTH_CREDS = json.loads(_OAUTH_CREDS)
else:
    DELEGATED_CREDS = os.environ["DELEGATED_CREDS"]
# configuration of the EHS sheet names
EHS_EMPL = os.environ["EHS_EMPL_SHEET"]
EHS_STUD = os.environ["EHS_STUD_SHEET"]

# Logging
TEN_MIB = 10 * 1024 * 1024
logger = logging.getLogger()
sh = logging.StreamHandler()
formatter = logging.Formatter("%(asctime)s %(name)-12s %(levelname)-8s %(message)s")
sh.setFormatter(formatter)
logger.addHandler(sh)
fh = RotatingFileHandler(
    os.getenv("LOGFILE", "email-poller.log"), maxBytes=TEN_MIB, backupCount=10
)
fh.setFormatter(formatter)
logger.addHandler(fh)
logger.setLevel(os.getenv("LOGLEVEL", default="INFO"))


def sheet_creds():
    scope = [
        "https://spreadsheets.google.com/feeds",
        "https://www.googleapis.com/auth/drive",
    ]
    credentials = service_account.Credentials.from_service_account_info(
        CREDS, scopes=scope
    )
    return credentials


def sheet_client():
    creds = sheet_creds()
    gc = gspread.authorize(creds)
    return gc


def authorize_gmail():
    creds = None
    scopes = [
        "https://www.googleapis.com/auth/gmail.readonly",
        "https://www.googleapis.com/auth/gmail.send",
    ]
    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if not PREPARE_OAUTH_CREDS:
        creds = Credentials.from_authorized_user_info(json.loads(DELEGATED_CREDS))
        service = build("gmail", "v1", credentials=creds, cache=MemoryCache())
        return service

    if os.path.exists(JSON_CREDS_CACHE):
        with open(JSON_CREDS_CACHE, "r") as fp:
            creds = Credentials.from_authorized_user_info(json.load(fp))
    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_config(OAUTH_CREDS, scopes)
            creds = flow.run_local_server(
                port=5000,
                open_browser=False,
                access_type="offline",
                prompt="consent",
                authorization_prompt_message="Please visit this URL: {url}",
            )
        # Save the credentials for the next run
        with open(JSON_CREDS_CACHE, "w") as fp:
            json.dump(json.loads(creds.to_json()), fp)

    service = build("gmail", "v1", credentials=creds, cache=MemoryCache())
    return service


class MemoryCache(Cache):
    """Hack for a breakage between oauth2client and google-api-client which
    apparently Google is unwilling to fix because of 'maintenance mode'.

    https://github.com/googleapis/google-api-python-client/issues/325

    This is ONLY required if using Gmail API, and this particular combination of
    libraries, oauth2client and google-api-python-client.
    """

    _CACHE = {}

    def get(self, url):
        return MemoryCache._CACHE.get(url)

    def set(self, url, content):
        MemoryCache._CACHE[url] = content


def download_attachment(service, user_id, msg_id, message_result, report_type):
    logger.info("processing email for EHS attachment")
    for part in message_result["payload"]["parts"]:
        if "parts" in part:
            for sub_part in part["parts"]:
                if sub_part["filename"]:
                    if "data" in sub_part["body"]:
                        data = sub_part["body"]["data"]
                    else:
                        att_id = sub_part["body"]["attachmentId"]
                        att = (
                            service.users()
                            .messages()
                            .attachments()
                            .get(userId=user_id, messageId=msg_id, id=att_id)
                            .execute()
                        )
                        data = att["data"]
                    file_data = base64.urlsafe_b64decode(data.encode("UTF-8"))
                    path = report_type + "-" + sub_part["filename"]
                    with open(path, "wb") as f:
                        f.write(file_data)
                    logger.info("attachment downloaded to path: %s", path)
                    # note that this return statement cheats this for loop. i am
                    # baking in the assumption that there is only one attachment
                    # in the EHS emails.
                    return path


def update_sheet(spreadsheet_name, df, worksheet_index=None):
    logger.info("updating spreadsheet: %s", spreadsheet_name)
    if worksheet_index is None:
        worksheet_index = 0
    gc = sheet_client()
    for x in gc.list_spreadsheet_files():
        logger.debug("service account access, confirmed for, %s", x["name"])
    ss = gc.open(spreadsheet_name)
    ws = ss.get_worksheet(worksheet_index)
    ws.update([df.columns.values.tolist()] + df.values.tolist())


def main():
    logger.info("beginning EHS attachment upload to gsheets process..")
    service = authorize_gmail()
    user_id = "me"

    latest_stud_report_received = False
    latest_empl_report_received = False
    student_email_subj = "EHS Reporting Data Student"
    employee_email_subj = "EHS Reporting Data Employee"

    # baked in assumption: messages come in from the API as most recent first.
    # therefore we will search through the emails until we find the first available
    # attachment for STUDENT or EMPLOYEE EHS excel files.
    results = service.users().messages().list(userId=user_id).execute()

    for message in results["messages"]:

        if latest_stud_report_received and latest_empl_report_received:
            # if we found the most recent reports, it's time to exit.
            return None

        msg_id = message["id"]
        # fetch the details for msg_id
        message_res = (
            service.users().messages().get(userId=user_id, id=msg_id).execute()
        )
        message_data = {}
        for header in message_res["payload"]["headers"]:
            message_data[header["name"]] = header["value"]
        received_ts = message_data["Received"].split(";")[1].strip()
        received_dt = parser.parse(received_ts)
        to_zone = tz.gettz("America/New_York")
        utcnow = datetime.now(tz.tzutc())
        estnow = utcnow.astimezone(to_zone)
        convert_dt = received_dt.astimezone(to_zone)
        minutes_since_last_update = (estnow - convert_dt).seconds / 60
        subject = message_data.get("Subject") or message_data.get("subject")
        if (
            not latest_stud_report_received
            and subject
            and subject == student_email_subj
        ):
            if minutes_since_last_update >= 90:
                logger.warning(
                    "EHS Student report is older than 90 minutes. Check the delegated aadmin email for potential issues."
                )
            logger.info(
                "dl-ing attachment from STUDENT email received at: %s",
                convert_dt.isoformat(),
            )
            path = download_attachment(service, user_id, msg_id, message_res, "student")
            df = pd.read_excel(path)
            df["UNI"] = df["Assignee"].apply(lambda x: x.split("-")[0].strip())
            update_sheet(EHS_STUD, df)
            latest_stud_report_received = True
        elif (
            not latest_empl_report_received
            and subject
            and subject == employee_email_subj
        ):
            if minutes_since_last_update >= 90:
                logger.warning(
                    "EHS Employee report is older than 90 minutes. Check the delegated aadmin email for potential issues."
                )
            logger.info(
                "dl-ing attachment from EMPLOYEE email received at: %s",
                convert_dt.isoformat(),
            )
            path = download_attachment(
                service, user_id, msg_id, message_res, "employee"
            )
            df = pd.read_excel(path)
            df["UNI"] = df["Assignee"].apply(lambda x: x.split("-")[0].strip())
            update_sheet(EHS_EMPL, df)
            latest_empl_report_received = True

    return None


if __name__ == "__main__":
    main()
