#!/usr/bin/env bash
export EHS_EMPL_SHEET=EHS-tracker-employee-DEV
export EHS_STUD_SHEET=EHS-tracker-student-DEV
export SERVICE_ACCOUNT_CREDS=$(cat ea-dev-proxy-api-service-account-creds.json)
export OAUTH_CREDS=$(cat ea-dev-proxy-api-oauth-client-creds.json)
export PREPARE_OAUTH_CREDS=1
export JSON_CREDS_CACHE=ea-dev-proxy-delegated-creds.json
python app.py

