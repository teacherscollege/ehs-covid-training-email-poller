# EHS COVID Training Email Poller

### High-level background

This project synchronizes PureSafety training data into the DHS system
via Google Spreadsheets API.

### Confluence Pages

- [EHS COVID Training Email Automation Runbook](https://teacherscollege.jira.com/wiki/spaces/WMA/pages/1875083319/EHS+COVID+Training+Email+Automation+Runbook)

### Structure of the code

* The main code is located in `app.py`.

### Scripts

1. `run-dev.sh`
2. `run-dev-ec2.sh`
3. `run-dev-oauth.sh`
4. `run-prod-ec2.sh`
5. `run-prod-oauth.sh`

Script #1 is used to run the code locally. It's a thin wrapper around
the python script `app.py`. It loads configuration files and runs the
app for the sake of local development.

Script #2 is used to run the app on the development ec2 instance.

Script #3 is used to regenerate the `DELEGATED_CREDS` should that ever
be needed (for the development environment).

Script #4 is used to run the app on the production ec2 instance.

Script #5 is used to regenerate the production `DELEGATED_CREDS`.

### Dependencies

### Built With

* Python
* See `requirements.txt` for Python libraries used.

## Getting Started

### Prerequisites for local development

* Python is installed
* Your system can run bash scripts

### Installation

#### Clone the repo

```
git clone git@bitbucket.org:teacherscollege/ehs-covid-training-email-poller.git
```

## Local Development 

### Set up the virtual environment

```
$ python3 -m venv venv
$ . venv/bin/activate
(venv) $ pip install -r requirements.txt 
```

### Set up the config files

1. Search Lastpass for "ea-dev-proxy-api@tc.columbia.edu".
2. Copy and paste the contents of `SERVICE_ACCOUNT_CREDS` into `ea-dev-proxy-api-service-account-creds.json`
3. Copy and paste the contents of `DELEGATED_CREDS` into `ea-dev-proxy-delegated-creds.json`

### Run the local development script

```
(venv) $ ./run-dev.sh
```

## Deployment

### Deployment strategy

See the "Deployment Procedure" in the runbook.

### Configuration

See the "Configuration" section of the runbook.
