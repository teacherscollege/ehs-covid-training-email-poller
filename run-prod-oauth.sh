#!/usr/bin/env bash
export EHS_EMPL_SHEET=EHS-tracker-employee-PROD
export EHS_STUD_SHEET=EHS-tracker-student-PROD
export SERVICE_ACCOUNT_CREDS=$(cat . ea-prod-proxy-api-service-account-creds.json)
export OAUTH_CREDS=$(cat ea-prod-proxy-api-oauth-client-creds.json)
export PREPARE_OAUTH_CREDS=1
export JSON_CREDS_CACHE=ea-prod-proxy-delegated-creds.json
python app.py

